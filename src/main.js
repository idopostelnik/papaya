import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Plugins
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)

// Bootstrap-vue package
import BootstrapVue from 'bootstrap-vue' 
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)

import apiUrl from '../utils/consts'

// Mixins
Vue.mixin({
  components: {
  },
  methods: {
    getData (path) {
      return new Promise ((resolve, reject) => {
        this.axios.get(apiUrl + path)
        .then((response) => {
          // console.log('getData response: ', response)
          resolve(response)
        })
        .catch((e) => {
          // console.log('getData e: ', e)
          reject(e)
        })
      })
    },
    postData (path, data) {
      return new Promise ((resolve, reject) => {
        this.axios.post(apiUrl + path, data)
        .then((response) => {
          // console.log('postData response: ', response)
          resolve(response)
        })
        .catch((e) => {
          // console.log('postData e: ', e)
          reject(e)
        })
      })
    },
    deleteData (path, data) {
      return new Promise ((resolve, reject) => {
        this.axios.delete(apiUrl + path, data)
        .then((response) => {
          // console.log('postData response: ', response)
          resolve(response)
        })
        .catch((e) => {
          // console.log('postData e: ', e)
          reject(e)
        })
      })
    },
    sellStock(symbol) {
      console.log('symbol: ', symbol)
      let data = {
        stockSymbol: symbol
      }
      this.postData('market/sell', data).then((response) => {
        console.log('response.data: ', response.data)
        location.reload()
      })
    }
  }
})

// style scss file
import '../scss/style.scss' 

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

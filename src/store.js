import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    followingArr: []
  },
  mutations: {
    ADD_STOCK (state, stock) {
      state.followingArr.push(stock)
    },
    REMOVE_STOCK (state, stock) {
      let index = state.followingArr.indexOf(stock)
      if (index > -1) {
        state.followingArr.splice(index, 1)
      }
    },
  },
  actions: {

  },
  plugins: [createPersistedState()]
})
